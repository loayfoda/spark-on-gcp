from dataproc_sparkdriver import submit_processing_join, submit_top_five_stations, submit_total_rides_perday
import logging
import logging.config

if __name__ == '__main__':
    logging.config.fileConfig('conf/logging.conf',disable_existing_loggers=False)
    log = logging.getLogger(__name__)
    submit_top_five_stations.top_five_startjob()
    submit_total_rides_perday.total_rides_startjob()
    submit_processing_join.join_start_job()
    log.info('All results are saved in Output folder')

