import sys,os
# needed for relative path import
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path)
import config
from dataproc_sparkdriver import sparkjob_definition, googlecloud_clients
import logging
log = logging.getLogger(__name__)
"""Submits the 'join job' to the dataproc cluster and waits till the job is executed to download the output from cloud storage.
The output is a csv file downloaded in the 'output' folder.
 To execute this module run 'python submit_processing_join.py'."""


def download_output(bucket):
    blobs = bucket.list_blobs(prefix='Output/join_result/')  # Get list of files
    for blob in blobs:
        filename = blob.name.replace('/', '_')
        blob.download_to_filename(config.join_local_name + filename)  # Download


def join_start_job():
    log.info('Processing join operation')
    dataproc = googlecloud_clients.get_client()
    job_id = sparkjob_definition.submit_pyspark_job(dataproc, config.project, config.region, config.cluster_name,
                                                    config.cloudstorage_bucket, 'Scripts/processing_join_sparkcode.py')

    result = googlecloud_clients.wait_for_job(dataproc, config.project, config.region, job_id)
    bucket = googlecloud_clients.get_storage_client()
    download_output(bucket)


# states to run the below mentioned function in case the class was explicitly run
if __name__ == '__main__':
    join_start_job()
