import sys,os
# needed for relative path import
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path)
import config
import sparkjob_definition, googlecloud_clients

"""Submits the 'join job' to the dataproc cluster and waits till the job is executed to download the output from GCS.
The out is a pie chart image downloaded in the 'output' folder.
To execute this module run 'python submit_top_five_stations.py'."""


def download_output(bucket):
    blob = bucket.blob(config.top_five_stations_output_path)
    blob.download_to_filename(config.top_five_stations_local_name)


def top_five_startjob():
    print('processing top five start stations used')
    dataproc = googlecloud_clients.get_client()
    job_id = sparkjob_definition.submit_pyspark_job(dataproc, config.project, config.region, config.cluster_name,
                                                    config.cloudstorage_bucket,
                                                    'Scripts/top_five_stations_sparkcode.py')

    result = googlecloud_clients.wait_for_job(dataproc, config.project, config.region, job_id)
    bucket = googlecloud_clients.get_storage_client()
    download_output(bucket)


# states to run the below mentioned function in case the class was explicitly run
if __name__ == '__main__':
    top_five_startjob()
