import sys,os
import config

# needed for relative path import
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path)
from dataproc_sparkdriver import sparkjob_definition, googlecloud_clients
import logging
log = logging.getLogger(__name__)
"""Submits the 'join job' to the dataproc cluster and waits till the job is executed to download the output from GCS.
The output is a bar chart image downloaded in the 'output' folder.
 To execute this module run 'python submit_total_rides_perday.py'. """


def download_output(bucket):
    blob = bucket.blob(config.total_rides_perday_output_path)
    blob.download_to_filename(config.total_rides_perday_local_name)


def total_rides_startjob():
    print('Processing total number of rides per day')
    dataproc = googlecloud_clients.get_client()
    job_id = sparkjob_definition.submit_pyspark_job(dataproc, config.project, config.region, config.cluster_name,
                                                    config.cloudstorage_bucket, 'Scripts/total_rides_perday.py')

    result = googlecloud_clients.wait_for_job(dataproc, config.project, config.region, job_id)
    bucket = googlecloud_clients.get_storage_client()
    download_output(bucket)


# states to run the below mentioned function in case the class was explicitly run
if __name__ == '__main__':
    total_rides_startjob()
