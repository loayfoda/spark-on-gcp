import os
import sys
# needed for relative path import
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path)
import config
from googleapiclient.discovery import build
from oauth2client.client import GoogleCredentials
from google.cloud import storage
import logging
log = logging.getLogger(__name__)

def get_client():
    """Builds a client to the dataproc API."""
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = config.google_credentials
    credentials = GoogleCredentials.get_application_default()
    dataproc = build('dataproc', 'v1', credentials=credentials)
    return dataproc


def wait_for_job(dataproc, project, region, job_id):
    """This method is used to wait for a spark a job till it successfully executed"""
    log.info('Waiting for job to finish...')
    while True:
        result = dataproc.projects().regions().jobs().get(
            projectId=project,
            region=region,
            jobId=job_id).execute()
        # Handle exceptions
        if result['status']['state'] == 'ERROR':
            raise Exception(result['status']['details'])
        elif result['status']['state'] == 'DONE':
            log.info('Job finished.\n')
            return result


def get_storage_client():
    """Creates a new cloud storage client"""
    storage_client = storage.Client.create_anonymous_client()
    bucket = storage_client.get_bucket(config.cloudstorage_bucket)
    return bucket
