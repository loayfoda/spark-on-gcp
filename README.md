# Appache spark on Google Dataproc

This project includes the code to execute different tasks throughout the data lifecycle
* Data Loading (python script)
* Data Cleaning (jupyter notebook)
* Data Processing
* Data Analysis 

## Project Architecture

The dataset and sparkjobs are saved in "Google Cloud Storage" which acts the storage basis needed by "Dataproc" service.
"Dataproc" is used to run teh spark jobs for analysing data. The data cleaning step is done using "jupyter notebook". 

The project has 4 main functionalities

* __Downloading the dataset :__
  * "download_bikers_data.py" : Download the dataset from the URL, Unzips the data, and uploads the unzipped file to "Google Cloud Storage" for further tasks. This script should be executed using the following command : 
    * python download_bikers_data.py
	
* __Data cleaining :__ is executed as an inteactive task using "jupyter notebook" to check the data the quality. This task downloads the dataset from the cloud storage, cleans the dataset and uploads the cleaned file for analysis.
  * "data:cleaning.ipynb" is the jupyter notebook including the different steps taken to clean the data. This task should be executed using "jupyter notebook"

* __Data processing and analysis__: The project includes 3 spark jobs. Each spark job is has 2 parts. The Spark job itself(inside the sparkcode package) and the drivers that are used to create a "DataProc" client to submit the spark jobs(dataproc-sparkdriver package) 
  * Get the top 5 start stations : retrieves the top 5 stations used by the customers. The spark job returns a bar chart representing the output which is being saved in the "cloud storage" and the "drivers" download this output in the "output" folder within the project 
  * Get the total number of rides per day: The spark job returns a pie chart representing the output which is being saved in the "cloud storage" and the "drivers class" download this output in the"output" folder within the project
  * Join operation: The spark job returns a "csv" file joining the dataset with the weather information dataset to retrieve the weather conditions of each day throughout the dataset along with the number of rides every day. The output is being saved in the "cloud storage" and the "drivers" download this output in the "output" folder within the project.
  
The three scripts yields the following visuals

![Top5-Stations-Used](/uploads/d62a7fb0ed56406466bb28af2fd0900c/Top5-Stations-Used.png)
![total_no_ride_perday](/uploads/db2364eda585c876bce761476abd2560/total_no_ride_perday.png)
![bydate](/uploads/fd0481717d97fbc9591314dcb2b4155b/bydate.PNG)


  # All the spark jobs can be executed separately as standalone or can be executed through running the "main.py" which executes all the spark jobs sequentially and saves the output in the "output" folder#
	
* __Clearing the jobs output :__ 
  * "clear_cloudstorage.py" : Clear the "Cloud Storage output directory" after the execution of the spark jobs. This script should be executed using the following command : 
    * python clear_cloudstorage.py



## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

*__Python Environment__
*__Google Cloud Project with with a cloud storage bucket and a Dataproc cluster__
*__Different libs used throughout the project__




### Installing
1. Unzip the file
2. Install the required packages


```
pip install  --user -r requirements.txt 
```
3. Create a Google cloud project --> create a new bucket and a Dataproc cluster --> Add the names in the config file
4. Create a new Google cloud service account with the Dataproc execution rights --> Add the service account to the "google_authentication" folder

5. Execute the "download_bikers_data.py" inside the "downloadfile" package(downloads, unzips and uploads the file to Clou Storage)

```
python download_bikers_data.py
```

6. Run the "data_cleaning.ipynb" using the "jupyter notebook" and execute every step within the file

7. Run the "main.py" to execute all three spark jobs using DataProc. This script execute all three spark jobs and downloads the output from the cloud storage to the "output" folder within the project

```
python main.py
```


1. Execute the "clear_cloudstorage.py" inside the "downloadfile"(clears the cloud storage output directory)

```
python clear_cloudstorage.py
```

## Built With

* [Spyder](https://www.spyder-ide.org/) - Python Development Environment
  
## Authors

* **Loay Foda** - *Initial work* - [Loay Foda](https://gitlab.com/loayfoda)


