
from pyspark.sql import SparkSession
from pyspark.sql.types import DateType
import pyspark.sql.functions as func

"""Joins the data set with the 'weather-info' set on the (date) to return a csv  with the weather condition for each 
day along with teh number rides('Date','number of rides' ' Weather conditions'). 
The output is saved in cloud storage bucket """


spark = SparkSession.builder.appName('join_task').getOrCreate()

data = spark.read.csv('gs://foda-bucket/CitiBikeData.csv', mode="DROPMALFORMED",inferSchema=True, header = True)
data = data.withColumn("record_date",data['Start Time'].cast(DateType()))

weather_data = spark.read.csv('gs://foda-bucket/Weather-info.csv', mode="DROPMALFORMED",inferSchema=True, header = True)
weather_data = weather_data.withColumn("weather_date",weather_data['Date'].cast(DateType()))

# grouping the  data by date to equalize both datasets granularity level, then joing both data sets and returning a
# csv file
data.groupby('record_date').count().join(weather_data, data.record_date == weather_data.weather_date).select('record_date',func.col("count").alias('Total Rides'),'Weather Type').orderBy('record_date', ascending=True).coalesce(1).write.option("header", "true").csv('gs://foda-bucket/Output/join_result')

