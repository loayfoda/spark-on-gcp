from io import BytesIO
from google.cloud import storage
from pyspark.sql import SparkSession
import matplotlib.pyplot as plt
import numpy as np

"""Creates a bar pie chart representing the top 5 most used Start Stations. 
   The output is saved in cloud storage bucket """


spark = SparkSession.builder.appName('topFiveStations').getOrCreate()
data = spark.read.csv('gs://foda-bucket/CitiBikeData.csv', mode="DROPMALFORMED", inferSchema=True, header=True)

# get the top 5 most used stations and plot them as a pie chart
top_five_stations = data.groupby('Start Station Name').count().orderBy('count', ascending=False).limit(5).toPandas()



y_pos=top_five_stations['count']
x_values=top_five_stations['Start Station Name']
x_pos = np.arange(len(x_values))

plt.figure(1, figsize=(17, 12))
plt.bar(x_pos, y_pos, align='center', alpha=0.5)
plt.xticks(x_pos, x_values,rotation=45)
plt.ylabel('Total Number of rides')
plt.title('Top 5 most start stations used')



# temporarily save image to buffer to write the image to cloud storage
buf = BytesIO()
plt.savefig(buf, format='png')

storage_client = storage.Client.create_anonymous_client()
bucket = storage_client.get_bucket('foda-bucket')
blob = bucket.blob('Output/top_five_stations.png')
# upload buffer contents to gcs
blob.upload_from_string(buf.getvalue(), content_type='image/png')

buf.close()

# gcs url to uploaded matplotlib image
url = blob.public_url

print(url)
