from pyspark.sql import SparkSession
from pyspark.sql.functions import date_format
from pyspark.sql.types import DateType
import numpy as np
import matplotlib.pyplot as plt
from google.cloud import storage
from io import BytesIO
"""Creates a new pie chart representing the total number of rides per day. 
   The output is saved in cloud storage bucket """


spark = SparkSession.builder.appName('total_rides_perday').getOrCreate()

data = spark.read.csv('gs://foda-bucket/CitiBikeData.csv', mode="DROPMALFORMED", inferSchema=True, header=True)

data = data.withColumn("record_day",date_format(data['Start Time'], 'EEEE')).withColumn("record_date",data['Start Time'].cast(DateType()))

total_no_rides = data.groupby('record_day').count().orderBy('count').toPandas()

plt.figure(1, figsize=(10, 7))
plt.pie(total_no_rides['count'], labels=total_no_rides['record_day'], autopct='%.1f%%', startangle=90)
plt.title('Total number of rides per day')

# temporarily save image to buffer to write the image to cloud storage
buf = BytesIO()
plt.savefig(buf, format='png')

storage_client = storage.Client.create_anonymous_client()
bucket = storage_client.get_bucket('foda-bucket')
blob = bucket.blob('Output/total_no_ride_perday.png')
# upload buffer contents to gcs
blob.upload_from_string(buf.getvalue(), content_type='image/png')
buf.close()
# gcs url to uploaded matplotlib image
url = blob.public_url

print(url)
