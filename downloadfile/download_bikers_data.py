import sys,os
from dataproc_sparkdriver import googlecloud_clients
from io import BytesIO
from zipfile import ZipFile
import requests
import config
import logging
import logging.config
# needed for relative path import
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path)
logging.config.fileConfig('../conf/logging.conf',disable_existing_loggers=False)
log = logging.getLogger(__name__)


"""This module downloads the zipped file, unzips it, and uploads the unzipped 'csv' to cloud storage for
 further processing using 'DataProc' service """

def download_bike_riders_data():
    """This method downloads the bikers data for "December 2016" and unzips the file.
    It reads the file 'URL' from the configuration file """
    log.info('Downloading the dataset ...')
    resp = requests.get(config.file_path)
    list_of_files = list()  # empty list
    with ZipFile(BytesIO(resp.content)) as zipcontent:
        zipfiles=zipcontent.namelist()
        for zipfile in zipfiles:
            list_of_files.append(zipcontent.open(zipfile))
        log.info('local dataset download complete')
        upload_bike_riders_data_to_cloud(list_of_files)


def upload_bike_riders_data_to_cloud(list_unzipped_files):
    """This method uploads the unzipped csv file to "Google Cloud Storage" for later use by spark jobs.
    It reads the bucket name from the configuration file"""
    # Instantiates a client
    log.info('Uploading the file to cloud storage.. ')
    bucket = googlecloud_clients.get_storage_client()
    for file in list_unzipped_files:
        blob = bucket.blob(file.name)
        blob.upload_from_file(file)
    log.info('Data is uploaded')


# states to run the below mentioned function in case the class was explicitly run
if __name__ == '__main__':
    download_bike_riders_data()



