import sys
sys.path.append('..')
import config
from dataproc_sparkdriver import googlecloud_clients
# needed for relative path import
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path)
"""This module is used to empty the output directory in cloud storage"""


def truncate_cloudstorage():
    """This method uses the storage client to delete all files within the output directory"""
    bucket = googlecloud_clients.get_storage_client()
    blobs = bucket.list_blobs(prefix=config.output_directory)
    for blob in blobs:
        print(blob.name)
        blob.delete()
    print('The Output folder is now empty')


# states to run the below mentioned function in case the class was explicitly run
if __name__ == '__main__':
    truncate_cloudstorage()

