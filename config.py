cloudstorage_bucket = 'foda-bucket'
file_path = 'https://s3.amazonaws.com/tripdata/201612-citibike-tripdata.zip'

cluster = 'foda-cluster-test'
zone = 'europe-west3-a'
input_file = 'gs://' + cloudstorage_bucket + '/CitiBikeData.csv'

input_file_local = "file:///SparkCourse/201612-citibike-tripdata.csv"
output_file_local = "file:///SparkCourse/result.csv"

project = 'foda-loay-gcp-training'
region = 'europe-west3'
cluster_name = 'foda-python-cluster'
processing_filename = 'processing_join'
output_directory = 'Output/'

join_output_file = 'join-result/'
join_output_path = output_directory + join_output_file
join_local_name = '../output/join_result'


top_five_stations_output_file = 'top_five_stations.png'
top_five_stations_output_path = output_directory + top_five_stations_output_file
top_five_stations_local_name = '../output/top_five_stations.png'

total_rides_perday_output_file = 'total_no_ride_perday.png'
total_rides_perday_output_path = output_directory + total_rides_perday_output_file
total_rides_perday_local_name = '../output/total_no_ride_perday.png'

google_credentials = '../google_authentication/credentials.json'